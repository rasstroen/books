<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160505171009 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('UPDATE genre SET title="детское" WHERE name="children"');
        $this->addSql('UPDATE genre SET title="детская проза" WHERE name="child_prose"');
        $this->addSql('UPDATE genre SET title="детская сказка" WHERE name="child_tale"');
        $this->addSql('UPDATE genre SET title="детский детектив" WHERE name="child_det"');
        $this->addSql('UPDATE genre SET title="детская фантастика" WHERE name="child_sf"');
        $this->addSql('UPDATE genre SET title="детские приключения" WHERE name="child_adv"');
        $this->addSql('UPDATE genre SET title="советская классика" WHERE name="prose_su_classics"');
        $this->addSql('UPDATE genre SET title="фантастика" WHERE name="sf_social"');
        $this->addSql('UPDATE genre SET title="юмористическая фантастика" WHERE name="sf_humor"');
        $this->addSql('UPDATE genre SET title="приключения" WHERE name="adventure"');
        $this->addSql('UPDATE genre SET title="образование" WHERE name="child_education"');
        $this->addSql('UPDATE genre SET title="географические приключения" WHERE name="adv_geo"');
        $this->addSql('UPDATE genre SET title="sf" WHERE name="sf"');
        $this->addSql('UPDATE genre SET title="4 года" WHERE name="child_4"');
        $this->addSql('UPDATE genre SET title="фантастика" WHERE name="sf_fantasy"');
        $this->addSql('UPDATE genre SET title="9 лет" WHERE name="child_9"');
        $this->addSql('UPDATE genre SET title="child_verse" WHERE name="child_verse"');
        $this->addSql('UPDATE genre SET title="десткие персонажи" WHERE name="child_characters"');
        $this->addSql('UPDATE genre SET title="биография" WHERE name="nonf_biography"');
        $this->addSql('UPDATE genre SET title="ref_encyc" WHERE name="ref_encyc"');
        $this->addSql('UPDATE genre SET title="научная история" WHERE name="sci_history"');
        $this->addSql('UPDATE genre SET title="классическая проза" WHERE name="prose_classic"');
        $this->addSql('UPDATE genre SET title="домашние животные" WHERE name="home_pets"');
        $this->addSql('UPDATE genre SET title="приключения с животными" WHERE name="adv_animal"');
        $this->addSql('UPDATE genre SET title="романтическая фантастика" WHERE name="romance_sf"');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
