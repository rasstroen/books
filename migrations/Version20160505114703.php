<?php namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20160505114703 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE IF EXISTS `book`');


        $this->addSql('CREATE TABLE `book` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `source_language_id` int(11) DEFAULT NULL,
  `annotation` text,
  `has_cover` tinyint(3) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`has_cover`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;');

        $this->addSql('DROP TABLE IF EXISTS `book_files`');

        $this->addSql('CREATE TABLE `book_files` (
  `book_id` bigint(20) NOT NULL DEFAULT \'0\',
  `file_id` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`book_id`,`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;');

        $this->addSql('DROP TABLE IF EXISTS `book_genres`');

        $this->addSql('CREATE TABLE `book_genres` (
  `book_id` bigint(20) NOT NULL DEFAULT \'0\',
  `genre_id` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`book_id`,`genre_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8');


        $this->addSql('DROP TABLE IF EXISTS `book_persons`');

        $this->addSql('CREATE TABLE `book_persons` (
    `book_id` bigint(20) NOT null DEFAULT \'0\',
  `person_id` int(11) NOT NULL DEFAULT \'0\',
  `role_id` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`book_id`,`person_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8');


        $this->addSql('DROP TABLE IF EXISTS `file`');

        $this->addSql('CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size` int(11) DEFAULT NULL,
  `file_type` enum(\'fb2\') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8');

        $this->addSql('DROP TABLE IF EXISTS `genre`');

        $this->addSql('CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8');
        $this->addSql('DROP TABLE IF EXISTS `person`');

        $this->addSql('CREATE TABLE `person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `first_name` (`first_name`,`last_name`,`middle_name`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8');

        $this->addSql('DROP TABLE IF EXISTS `user`');

        $this->addSql('CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `network_user_id` bigint(20) DEFAULT NULL,
  `network_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `network_id` (`network_id`,`network_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
