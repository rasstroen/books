<?php
require __DIR__ . '/vendor/autoload.php';
require 'recipe/common.php';
serverList('.deploy/servers.yml');
set('repository', 'git@bitbucket.org:rasstroen/books.git');
set('keep_releases', 2);
// dirs, shared between releases
set('shared_dirs', [
    'upload',
]);
// files, shared between releases
set('shared_files', [
    '.env',
]);
// writable dir for cache, etc
set('writable_dirs', [
    'templates/cache',
]);
set('writable_use_sudo', false);

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
    'cleanup',
])->desc('Deploying project.');
after('deploy', 'success');

// update environment
task('upload:env', function () {
    upload('.deploy/.env.' . env('server.name'), '{{deploy_path}}/shared/.env');
});

// run migrations
task('deploy:migrations', function () {
    run('echo migrations...');
})->desc('Running migrations.');

before('deploy:shared', 'upload:env');
after('deploy:shared', 'deploy:migrations');
