## Infrastructure
### Deploy
1. Install [deployer tool](http://deployer.org)
2. Run
```
dep deploy production
```

## Commands
### Parse

to parse existing books one more time:
php plumbus updateBooks

to parse existing book one more time:
php plumbus updateBooks --bookId=1

