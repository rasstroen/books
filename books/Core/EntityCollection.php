<?php namespace Books\Core;

use Books\Core\Entity\Basic;
use Books\Traits\IdentityMapTrait;

abstract class EntityCollection
{
    use IdentityMapTrait;
    public function getById(int $id)
    {
        $books = $this->getByIds([$id]);
        return !empty($books) ? reset($books) : null;
    }

    /**
     * @param array $ids
     * @return Basic[]
     */
    public function getByIds(array $ids):array
    {
        $toFetch = $ids;
        $this->checkInIdentityMap($toFetch);
        $this->checkInCache($toFetch);
        $this->checkInStorages($toFetch);
        return $this->getFromIdentityMap($ids);
    }

    protected function checkInIdentityMap(array &$ids)
    {

    }

    protected function checkInCache(array &$ids)
    {

    }

    abstract protected function getFromIdentityMap(array $ids):array;

    abstract protected function checkInStorages(array &$ids);
}
