<?php namespace Books\Core\Util;

/**
 * @todo move to core repository
 *
 * Class UtilString
 * @package Books\Core\Util
 */
class UtilString
{

    public static function transliterate(string $text, string $charset = null)
    {
        $charset = ($charset === null ? mb_internal_encoding() : $charset);
        static $transliterationMap;
        $result = '';
        $textLength = mb_strlen($text, $charset);

        if ($transliterationMap === null) {
            $transliterationMap = array(
                1040 => 'A',
                1041 => 'B',
                1042 => 'V',
                1043 => 'G',
                1044 => 'D',
                1045 => 'E',
                1025 => 'Yo',
                1046 => 'Zh',
                1047 => 'Z',
                1048 => 'I',
                1049 => 'J',
                1050 => 'K',
                1051 => 'L',
                1052 => 'M',
                1053 => 'N',
                1054 => 'O',
                1055 => 'P',
                1056 => 'R',
                1057 => 'S',
                1058 => 'T',
                1059 => 'U',
                1060 => 'F',
                1061 => 'Kh',
                1062 => 'Cz',
                1063 => 'Ch',
                1064 => 'Sh',
                1065 => 'Shh',
                1066 => '',
                1067 => 'Y',
                1068 => '',
                1069 => 'E',
                1070 => 'Yu',
                1071 => 'Ya',
                1072 => 'a',
                1073 => 'b',
                1074 => 'v',
                1075 => 'g',
                1076 => 'd',
                1077 => 'e',
                1105 => 'yo',
                1078 => 'zh',
                1079 => 'z',
                1080 => 'i',
                1081 => 'j',
                1082 => 'k',
                1083 => 'l',
                1084 => 'm',
                1085 => 'n',
                1086 => 'o',
                1087 => 'p',
                1088 => 'r',
                1089 => 's',
                1090 => 't',
                1091 => 'u',
                1092 => 'f',
                1093 => 'kh',
                1094 => 'cz',
                1095 => 'ch',
                1096 => 'sh',
                1097 => 'shh',
                1098 => '',
                1099 => 'y',
                1100 => '',
                1101 => 'e',
                1102 => 'yu',
                1103 => 'ya',
                8217 => '',
                8470 => '#',
            );
        }

        for ($i = 0; $i < $textLength; $i++) {
            $character = mb_substr($text, $i, 1, $charset);
            $ord = self::uniOrd($character, $charset);

            if (isset($transliterationMap[$ord])) {
                $result .= $transliterationMap[$ord];
            } else {
                $result .= $character;
            }
        }

        return $result;
    }

    /**
     * Return UTF-8 value of character. Safe for multi byte characters.
     *
     * @param string $character The input character
     * @param string $charset Character set of <tt>$character</tt>
     *
     * @return integer The UTF-8 value as an integer
     */
    public static function uniOrd(string $character, string $charset = null)
    {
        $charset = ($charset === null ? mb_internal_encoding() : $charset);

        if (!self::areCharsetsEqual($charset, 'utf-8')) {
            $character = mb_convert_encoding($character, 'utf-8', $charset);
        }

        $character = mb_substr($character, 0, 1, 'utf-8');
        $size = strlen($character);
        $ord = ord($character[0]) & (0xFF >> $size);

        //Merge other characters into the value
        for ($i = 1; $i < $size; $i++) {
            $ord = $ord << 6 | (ord($character[$i]) & 127);
        }

        return $ord;
    }

    /**
     * Checks if <tt>$charset1</tt> equals to <tt>$charset2</tt>
     * @param string $charset1
     * @param string $charset2
     *
     * @return boolean
     *
     * @throws InvalidArgumentException
     */
    public static function areCharsetsEqual(string $charset1, string $charset2)
    {
        $charset1 = trim(mb_strtolower($charset1));
        $charset2 = trim(mb_strtolower($charset2));

        if (!function_exists('mb_encoding_aliases')) {
            $charset1 = preg_replace('/[^a-z0-9]/', '', $charset1);
            $charset2 = preg_replace('/[^a-z0-9]/', '', $charset2);

            return $charset1 === $charset2;
        }

        return (bool) array_intersect(
            array_values(
                array_unique(
                    array_merge(
                        array($charset1),
                        mb_encoding_aliases($charset1)
                    ),
                    SORT_STRING
                )
            ),
            array_values(
                array_unique(
                    array_merge(
                        array($charset2),
                        mb_encoding_aliases($charset2)
                    ),
                    SORT_STRING
                )
            )
        );
    }
}
