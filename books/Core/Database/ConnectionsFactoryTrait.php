<?php namespace Books\Core\Database;

trait ConnectionsFactoryTrait
{
    /**
     * @return ConnectionsFactory
     */
    public function getConnectionsFactory()
    {
        return ConnectionsFactory::instance();
    }
}
