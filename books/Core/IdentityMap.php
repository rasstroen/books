<?php namespace Books\Core;

use Plumbus\Injectable\InjectableComponentTrait;
use Books\Core\Entity\Basic;

final class IdentityMap
{
    use InjectableComponentTrait;
    /**
     * @var array
     */
    private $storage = [];

    public function add(Basic $entity)
    {
        $entityClass = \get_class($entity);
        $id = $entity->getId();
        $this->storage[$entityClass][$id] = $entity;
    }

    public function getById($id, $entityClass)
    {
        return isset($this->storage[$entityClass][$id]) ? $this->storage[$entityClass][$id] : null;
    }
}
