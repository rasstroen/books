<?php namespace Books\Core\Page;

use Books\Traits\BooksCollectionTrait;
use Plumbus\Core\Controller\Page\SimplePage;
use Plumbus\Exception\NotFound;

class BookPage extends SimplePage
{
    use BooksCollectionTrait;

    public function getTitle():string
    {
        $name = $this->getVariable('name');
        if (!$name) {
            throw new NotFound('Illegal book name');
        }

        $transliteratedTitleArray = explode('-', $name);
        $bookId = (int) array_pop($transliteratedTitleArray);
        $book = $this->getBooksCollection()->getById($bookId);

        if (!$book) {
            throw new NotFound('No book found in base');
        }
        $title = [];
        $authors = $book->getAuthors();
        foreach ($authors as $author) {
            $title[] = $author->getFirstName() . ' ' . $author->getMiddleName() . ' ' . $author->getLastName();
        }
        return implode(', ', $title) . ' ' . $book->getTitle();
    }
}