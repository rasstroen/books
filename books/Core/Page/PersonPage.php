<?php namespace Books\Core\Page;

use Books\Core\Entity\Author;
use Books\Traits\BLL\AuthorsBLLTrait;
use Plumbus\Core\Controller\Page\SimplePage;
use Plumbus\Exception\NotFound;

class PersonPage extends SimplePage
{
    use AuthorsBLLTrait;

    public function getTitle():string
    {
        $personId = $this->getVariable('personId');
        if (!$personId) {
            throw new NotFound('Illegal person id');
        }

        $row = $this->getAuthorsBll()->getById($personId);
        if (!$row) {
            throw new NotFound('No person found in base');
        }

        $person = new Author();

        $person->setFirstName($row['first_name']);
        $person->setMiddleName($row['middle_name']);
        $person->setLastName($row['last_name']);

        return $person->getFirstName() . ' ' . $person->getMiddleName() . ' ' . $person->getLastName();
    }
}