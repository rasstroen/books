<?php namespace Books\Core\Page;

use Books\Core\Entity\Author;
use Books\Core\Entity\Genre;
use Books\Traits\BLL\AuthorsBLLTrait;
use Books\Traits\BLL\GenresBLLTrait;
use Plumbus\Core\Controller\Page\SimplePage;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Exception\NotFound;

class GenrePage extends SimplePage
{
    use GenresBLLTrait;

    public function getTitle():string
    {
        $genreName = (string) trim($this->getVariable('genreName'));
        if (!$genreName) {
            throw new NotFound('Illegal genre name');
        }

        $filter = new Filter();
        $filter->setCondition('name', $genreName);

        $row = $this->getGenresBll()->getByFilter($filter);

        if (!empty($row)) {
            $row = reset($row);
        }

        if (!$row) {
            throw new NotFound('No genre ' . $genreName . ' found in base');
        }

        $genre = new Genre();

        $genre->setName($row['name']);
        $genre->setTitle($row['title']);


        return 'книги жанра "' . ($genre->getTitle() ?? $genre->getName()) . '""';
    }
}