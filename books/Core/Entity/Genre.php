<?php
namespace Books\Core\Entity;

use Books\Core\Util\UtilString;
use Doctrine\DBAL\Types\Type;

class Genre extends Basic
{

    public function getId()
    {
        return $this->getGenreId();
    }

    /**
     * @var int
     */
    protected $genreId;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $name;

    public function getUrl()
    {
        return '/genre/' . UtilString::transliterate($this->getName());
    }


    public static function getMetaFields():array
    {
        return [
            'genreId' => ['type' => Type::BIGINT, 'field' => 'id'],
            'title' => ['type' => Type::STRING, 'field' => 'title'],
            'name' => ['type' => Type::STRING, 'field' => 'name'],
        ];
    }

    public static function getIndexField():string
    {
        return 'id';
    }

    public static function getMetaTableName():string
    {
        return 'genre';
    }

    /**
     * @return int
     */
    public function getGenreId()
    {
        return $this->genreId;
    }

    /**
     * @param int $genreId
     */
    public function setGenreId($genreId)
    {
        $this->genreId = $genreId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}