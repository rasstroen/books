<?php
namespace Books\Core\Entity;

use Doctrine\DBAL\Types\Type;

abstract class File extends Basic
{
    protected $fileType;

    /**
     * @param string $filesDirectory
     * @return string
     */
    public function getHtmlFilePath(string $filesDirectory) : string
    {
        $directory = $filesDirectory . DIRECTORY_SEPARATOR . substr(md5($this->getFileId() . 'html'), 1,
                4);
        $fileName = $this->getFileId() . '.' . $this->fileType;
        return $directory . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param string $filesDirectory
     * @return string
     */
    public function getFilePath(string $filesDirectory) : string
    {
        $directory = $filesDirectory . DIRECTORY_SEPARATOR . substr(md5($this->getFileId() . $this->getFileType()), 1,
                4);
        $fileName = $this->getFileId() . '.' . $this->fileType;
        return $directory . DIRECTORY_SEPARATOR . $fileName;
    }

    public function getFileUrl()
    {
        $path = '/upload/' . substr(md5($this->getFileId() . $this->getFileType()), 1, 4);
        $fileName = $this->getFileId() . '.' . $this->fileType;
        return $path . '/' . $fileName;
    }

    public function getId()
    {
        return $this->getFileId();
    }

    public static function getMetaFields():array
    {
        return [
            'fileId' => ['type' => Type::BIGINT, 'field' => 'id'],
            'fileType' => ['type' => Type::STRING, 'field' => 'file_type'],
            'size' => ['type' => Type::STRING, 'field' => 'size']
        ];
    }

    public static function getIndexField():string
    {
        return 'id';
    }

    public static function getMetaTableName():string
    {
        return 'file';
    }

    /**
     * @var int
     */
    protected $fileId;

    /**
     * @var int
     */
    protected $size;

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * @param string $fileType
     */
    public function setFileType(string $fileType)
    {
        $this->fileType = (string) $fileType;
    }

    /**
     * @return int
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @param int $fileId
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }
}