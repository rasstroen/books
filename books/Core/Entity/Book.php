<?php namespace Books\Core\Entity;

use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Books\Core\Entity\File\FB2;
use Books\Core\FB2\MetaData;
use Books\Core\FB2\Parser;
use Books\Core\Util\UtilString;
use Books\Traits\BLL\AuthorsBLLTrait;
use Books\Traits\BLL\FilesBLLTrait;
use Books\Traits\BLL\GenresBLLTrait;
use Doctrine\DBAL\Types\Type;

class Book extends Basic
{
    use AuthorsBLLTrait;
    use FilesBLLTrait;
    use GenresBLLTrait;
    use ProjectConfigurationTrait;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getBookId();
    }

    /**
     * @return string
     */
    public function getCoverUrl()
    {
        if ($this->getHasCover()) {
            $path = '/upload/' . substr(md5($this->getId()), 1, 4);
            $fileName = $this->getId() . '.jpg';
            return $path . '/' . $fileName;
        }
        /**
         * @todo
         */
        return '/static/css/olympia/images/images/product-grid.jpg';
    }

    /**
     * @param $filesDirectory
     * @return string
     */
    public function getCoverPath(string $filesDirectory)
    {
        $directory = $filesDirectory . DIRECTORY_SEPARATOR . substr(md5($this->getId()), 1, 4);
        $fileName = $this->getId() . '.jpg';
        return $directory . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @return string
     */
    public function getReadUrl()
    {
        return '/book/read/' . UtilString::transliterate(str_replace(' ', '_',
            $this->getTitle())) . '-' . $this->getId();
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '/book/' . UtilString::transliterate(str_replace(' ', '_', $this->getTitle())) . '-' . $this->getId();
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        $files = $this->getFiles();
        foreach ($files as $file) {
            if ($file instanceof FB2) {
                /**
                 * @var FB2 $file
                 */
                $path = $file->getFilePath(
                    $this->configuration()->getSetting('pathes.upload.books')
                );
                $htmlPath = $file->getHtmlFilePath(
                    $this->configuration()->getSetting('pathes.upload.books')
                );
                $parser = new Parser();
                return $parser->getHTML($path, $htmlPath);
            }
        }
        return '';
    }

    /**
     * @var string
     */
    protected $isbn;

    /**
     * @var Author[]
     */
    protected $authors;

    /**
     * @var Translator[]
     */
    protected $translators;
    /***
     * @var Genre[]
     */
    protected $genres;

    /**
     * @var bool
     */
    protected $hasCover;

    /**
     * @return Author[]
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @return Translator[]
     */
    public function getTranslators()
    {
        return $this->translators;
    }

    /**
     * @param Author $person
     */
    public function addAuthor(Author $author)
    {
        $this->authors[$author->getId()] = $author;
    }

    public function addTranslator(Author $translator)
    {
        $this->translators[$translator->getId()] = $translator;
    }

    /**
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @param array $genres
     */
    public function setGenres(array $genres)
    {
        $this->genres = (array) $genres;
    }

    /**
     * @param Genre $genre
     */
    public function addGenre(Genre $genre)
    {
        $this->genres[$genre->getId()] = $genre;
    }

    /**
     * @return Genre[]
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @return boolean
     */
    public function getHasCover()
    {
        return (int) $this->hasCover;
    }

    /**
     * @param boolean $hasCover
     */
    public function setHasCover($hasCover)
    {
        $this->hasCover = (bool) $hasCover;
    }

    /**
     * @param array $persons
     */
    public function setAuthors(array $persons)
    {
        $this->authors = (array) $persons;
    }

    /**
     * @param array $persons
     */
    public function setTranslators(array $persons)
    {
        $this->translators = (array) $persons;
    }

    public function save()
    {
        /**
         * Сохраняем книгу
         */
        $bookId = parent::save();
        if ($bookId) {
            $this->setBookId($bookId);
        }
        /**
         * Сохраняем связи с авторами
         */
        foreach ($this->authors as $author) {
            $this->getAuthorsBll()->setBookLink($this->getBookId(), $author->getPersonId(), $author->getRole());
        }
        foreach ($this->translators as $translator) {
            $this->getAuthorsBll()->setBookLink($this->getBookId(), $translator->getPersonId(), $translator->getRole());
        }
        /**
         * Сохраняем связи с файлами
         */
        foreach ($this->getFiles() as $file) {
            $this->getFilesBll()->setBookLink($this->getBookId(), $file->getFileId());
        }
        /**
         * Сохраняем связи с жанрами
         */
        foreach ($this->getGenres() as $genre) {
            $this->getGenresBll()->setBookLink($this->getBookId(), $genre->getGenreId());
        }
    }

    public static function getMetaFields():array
    {
        return [
            'bookId' => ['type' => Type::BIGINT, 'field' => 'id'],
            'title' => ['type' => Type::STRING, 'field' => 'title'],
            'languageId' => ['type' => Type::INTEGER, 'field' => 'language_id'],
            'sourceLanguageId' => ['type' => Type::INTEGER, 'field' => 'source_language_id'],
            'annotation' => ['type' => Type::STRING, 'field' => 'annotation'],
            'hasCover' => ['type' => Type::INTEGER, 'field' => 'has_cover'],
            'isbn' => ['type' => Type::STRING, 'field' => 'isbn'],
        ];
    }

    public static function getIndexField():string
    {
        return 'id';
    }

    public static function getMetaTableName():string
    {
        return 'book';
    }

    public static function getMetaRelations():array
    {
        return [
            File::class => [
                'table' => 'book_files',
                'field' => 'book_id',
                'relation_field' => 'file_id',
            ],
            Author::class => [
                'table' => 'book_persons',
                'field' => 'book_id',
                'relation_field' => 'person_id',
                'additional_fields' => [
                    'bookRoleId' => ['field' => 'role_id']
                ]
            ],
            Genre::class => [
                'table' => 'book_genres',
                'field' => 'book_id',
                'relation_field' => 'genre_id',
            ]
        ];
    }

    /**
     * @var int
     */
    protected $bookId;
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $annotation;

    /**
     * @var int
     */
    protected $languageId;

    /**
     * @var int
     */
    protected $sourceLanguageId;

    /**
     * @var File[]
     */
    protected $files;

    /**
     * @return int
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * @param int $bookId
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;
    }

    public function fillByFB2MetaData(MetaData $metaData)
    {
        $this->setTitle($metaData->getTitle());
        $this->setLanguageId($metaData->getLanguageId());
        $this->setSourceLanguageId($metaData->getSourceLanguageId());
        $this->setAnnotation($metaData->getAnnotation());
        $this->setIsbn($metaData->getIsbn());
    }

    /**
     * @return File[]
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param File[] $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @param File $file
     */
    public function addFile(File $file)
    {
        $this->files[$file->getId()] = $file;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * @param string $annotation
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = (string) $annotation;
    }

    /**
     * @param int $sourceLanguageId
     */
    public function setSourceLanguageId($sourceLanguageId)
    {
        $this->sourceLanguageId = $sourceLanguageId;
    }

    /**
     * @return int
     */
    public function getSourceLanguageId()
    {
        return $this->sourceLanguageId;
    }

    public function getSourceLanguageCode()
    {
        return $this->configuration()->getSetting('languages_id.' . $this->getSourceLanguageId());
    }

    public function getSourceLanguageName()
    {
        return $this->configuration()->getSetting('languages_translations.' . $this->getSourceLanguageCode());
    }

    /**
     * @param int $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;
    }

    /**
     * @return int
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    public function getLanguageName()
    {
        $lang = $this->configuration()->getSetting('languages_id.' . $this->getLanguageId());
        return $this->configuration()->getSetting('languages_translations.' . $lang);
    }
}
