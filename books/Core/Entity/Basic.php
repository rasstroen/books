<?php namespace Books\Core\Entity;

use Books\Core\Database\ConnectionsFactoryTrait;

abstract class Basic
{
    use ConnectionsFactoryTrait;

    abstract public function getId();

    public function fromArray(array $data)
    {
        foreach ($data as $field => $value) {
            $setter = 'set' . $field;
            $this->$setter($value);
        }
    }

    public function save()
    {
        foreach ($this->getMetaFields() as $propertyName => $field) {
            $getter = 'get' . $propertyName;
            $values[$field['field']] = $this->$getter();
        }
        $this->getConnectionsFactory()->web->insert($this->getMetaTableName(), $values, array_keys($values));
        return $this->getConnectionsFactory()->web->lastInsertId();
    }

    abstract public static function getMetaFields():array;

    abstract public static function getIndexField():string;

    abstract public static function getMetaTableName():string;

    public static function getMetaRelations():array
    {
        return [];
    }
}
