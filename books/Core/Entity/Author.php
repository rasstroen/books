<?php namespace Books\Core\Entity;

use Doctrine\DBAL\Types\Type;

class Author extends Basic
{
    const ROLE_AUTHOR = 1;
    const ROLE_TRANSLATOR = 2;

    protected $roleType = self::ROLE_AUTHOR;

    public function getId()
    {
        return $this->getPersonId();
    }

    public function getUrl()
    {
        return '/person/' . $this->getId();
    }

    public function getRole()
    {
        return $this->roleType;
    }

    public function setBookRoleId($roleId)
    {
        $this->roleType = $roleId;
    }

    /**
     * @var int
     */
    protected $personId;

    /**
     * @var string
     */
    protected $firstName;
    /**
     * @var string
     */
    protected $lastName;
    /**
     * @var string
     */
    protected $middleName;

    public static function getMetaFields():array
    {
        return [
            'personId' => ['type' => Type::BIGINT, 'field' => 'id'],
            'firstName' => ['type' => Type::STRING, 'field' => 'first_name'],
            'middleName' => ['type' => Type::STRING, 'field' => 'middle_name'],
            'lastName' => ['type' => Type::STRING, 'field' => 'last_name'],
        ];
    }

    public static function getIndexField():string
    {
        return 'id';
    }

    public static function getMetaTableName():string
    {
        return 'person';
    }

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $authorId
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }
}