<?php
namespace Books\Core\Entity\File;

use Books\Core\Entity\File;

class FB2 extends File
{
    public $fileType = 'fb2';
}