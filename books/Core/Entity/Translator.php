<?php
namespace Books\Core\Entity;

class Translator extends Author
{
    protected $roleType = Author::ROLE_TRANSLATOR;
}