<?php namespace Books\Module\Html;

use Books\Traits\BLL\GenresBLLTrait;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Traits\Classes\User\CurrentUserTrait;

class Text extends \Plumbus\Core\Module\Base
{
    use CurrentUserTrait;
    use GenresBLLTrait;

    public function actionShowTopMenu()
    {
        $genres = $this->getGenresBll()->getByFilter(new Filter());
        $grouped = [];
        $i = 0;
        foreach ($genres as $genre) {
            $genre['title'] = $genre['title'] ?? $genre['name'];
            $grouped[$i++ % 4][] = $genre;
        }
        return ['genres' => $grouped];
    }

    public function actionShowFooter()
    {
        return [];
    }

    public function actionShowTextOnMain()
    {
        return [];
    }

    public function actionShowTextOnMyProfile()
    {
        return [
            'user' => $this->getCurrentUser()->getUser()
        ];
    }
}
