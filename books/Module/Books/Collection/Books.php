<?php
namespace Books\Module\Books\Collection;

use Plumbus\Injectable\InjectableComponentTrait;
use Books\Core\Entity\Author;
use Books\Core\Entity\Basic;
use Books\Core\Entity\Book;
use Books\Core\Entity\File;
use Books\Core\Entity\File\FB2;
use Books\Core\Entity\Genre;
use Books\Core\EntityCollection;
use Books\Traits\BLL\BooksBLLTrait;

class Books extends EntityCollection
{
    use InjectableComponentTrait;
    use BooksBLLTrait;

    /**
     * @param array $ids
     * @return Book[]
     */
    public function getByIds(array $ids):array
    {
        return parent::getByIds($ids);
    }

    /**
     * @param int $id
     * @return Book|null
     */
    public function getById(int $bookId)
    {
        return parent::getById($bookId);
    }

    protected function checkInStorages(array &$ids)
    {
        $books = $this->getBooksBll()->getAllByIds($ids);

        foreach ($books as $bookRow) {
            $entities = $this->hydrateRow($bookRow);
            $bookIdField = Book::class . '::bookId';
            $book = $this->getIdentityMap()->getById($bookRow[$bookIdField], Book::class);
            /**
             * @var Book $book
             */
            foreach ($entities as $entity) {
                /**
                 * @var Basic $entity
                 */
                switch (get_class($entity)) {
                    case Author::class:
                        /**
                         * @var Author $entity
                         */
                        switch ($entity->getRole()) {
                            case Author::ROLE_AUTHOR:
                                $book->addAuthor($entity);
                                break;
                            case Author::ROLE_TRANSLATOR:
                                $book->addTranslator($entity);
                                break;
                        }
                        break;
                    case Genre::class:
                        $book->addGenre($entity);
                        break;
                    case FB2::class:
                        $book->addFile($entity);
                        break;
                }
            }
            $this->getIdentityMap()->add($book);
        }
    }

    protected function getFromIdentityMap(array $ids):array
    {
        $entities = [];
        foreach ($ids as $id) {
            $entity = $this->getIdentityMap()->getById($id, Book::class);
            if ($entity) {
                $entities[] = $entity;
            }
        }
        return $entities;
    }

    public function hydrateRow(array $row)
    {
        $entities = [];
        foreach ($row as $field => $value) {
            list($entityClass, $entityFieldName) = explode('::', $field);
            $entityRow[$entityClass][$entityFieldName] = $value;
        }

        foreach ($entityRow as $entityClass => $data) {
            switch ($entityClass) {
                case File::class:
                    switch ($data['fileType']) {
                        case 'fb2':
                            $entity = new FB2();
                            break;
                    }
                    break;
                default:
                    $entity = new $entityClass;
                    break;
            }

            /**
             * @var Basic $entity
             */
            $entity->fromArray($data);
            if (!$this->getIdentityMap()->getById($entity->getId(), $entityClass)) {
                $this->getIdentityMap()->add($entity);
            }
            $entities[] = $entity;
        }
        return $entities;
    }
}