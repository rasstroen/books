<?php
namespace Books\Module\Books;

use Plumbus\Core\Module\Base;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Core\Module\BLL\Paging;
use Plumbus\Exception\NotFound;
use Plumbus\Exception\Redirect;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Plumbus\Traits\Core\RequestTrait;
use Books\Core\Entity\Author;
use Books\Core\Entity\Book;
use Books\Core\Entity\File\FB2;
use Books\Core\Entity\Genre;
use Books\Core\Entity\Translator;
use Books\Core\FB2\MetaData;
use Books\Core\Util\UtilString;
use Books\Traits\BLL\AuthorsBLLTrait;
use Books\Traits\BLL\BooksBLLTrait;
use Books\Traits\BLL\FilesBLLTrait;
use Books\Traits\BLL\GenresBLLTrait;
use Books\Traits\BooksCollectionTrait;
use Books\Traits\FB2ParserTrait;
use Sphinx\SphinxClient;

class Books extends Base
{
    use RequestTrait;
    use FB2ParserTrait;
    use FilesBLLTrait;
    use ProjectConfigurationTrait;
    use AuthorsBLLTrait;
    use GenresBLLTrait;
    use BooksCollectionTrait;
    use BooksBLLTrait;

    /**
     * На главной выводятся популярные за последние POPULAR_BOOKS_ON_MAIN_PERIOD_DAYS дней книги
     */
    const POPULAR_BOOKS_ON_MAIN_PERIOD_DAYS = 7;

    public function actionReadBook(array $variables = null)
    {
        return $this->actionShowBook($variables);
    }

    /**
     * Страница книги
     *
     * @param array|null $variables
     * @return array
     * @throws NotFound
     * @throws Redirect
     */
    public function actionShowBook(array $variables = null)
    {
        $name = $variables['name'] ?? null;
        if (!$name) {
            throw new NotFound('Illegal book name');
        }

        $transliteratedTitleArray = explode('-', urldecode($name));
        $bookId = (int) array_pop($transliteratedTitleArray);
        $transliteratedTitle = implode('-', $transliteratedTitleArray);

        $book = $this->getBooksCollection()->getById($bookId);

        if (!$book) {
            throw new NotFound('No book found in base');
        }

        /**
         * Редиректим на правильный урл, чтобы поисковики не индексировали 1 книгу по многим урлам
         */
        if ($transliteratedTitle !== UtilString::transliterate(str_replace(' ', '_', $book->getTitle()))) {
            $redirectException = new Redirect();
            $redirectException->setRedirectUri($book->getUrl());
            throw $redirectException;
        }

        return [
            'book' => $book
        ];
    }

    public function actionShowPersonBooks(array $variables = null)
    {
        $personId = $variables['personId'] ?? null;
        if (!$personId) {
            throw new NotFound('Illegal person id');
        }

        $row = $this->getAuthorsBll()->getById($personId);
        if (!$row) {
            throw new NotFound('No person found in base');
        }

        $paging = new Paging();
        $perPage = 10;
        $currentPage = $this->getRequest()->getQueryParam('page', 1);
        $paging->setPerPage($perPage);
        $paging->setPage($currentPage);

        $totalCount = 0;
        $booksIds = $this->getBooksBll()->getIdsByPersonId($personId, $paging, $totalCount);


        $paging->setTotalItemsCount($totalCount);
        $books = $this->getBooksCollection()->getByIds($booksIds);

        $pages = $paging->getPrettyPages(2, 2, 2);
        $pagerLinks = [];

        foreach ($pages as $page) {
            $pagerLinks[$page] = [
                'link' => '?page=' . $page,
                'id' => $page
            ];
            if ($page == $currentPage) {
                $pagerLinks[$page]['current'] = true;
            }
        }


        return [
            'books' => $books,
            'pageLinks' => $pagerLinks ?? null,
        ];
    }

    public function actionShowGenreBooks(array $variables = null)
    {
        $genreName = (string) trim($variables['genreName'] ?? '');
        if (!$genreName) {
            throw new NotFound('Illegal genre name');
        }

        $filter = new Filter();
        $filter->setCondition('name', $genreName);
        $row = $this->getGenresBll()->getByFilter($filter, $totalCount);

        if (!empty($row)) {
            $row = reset($row);
        }
        if (!$row) {
            throw new NotFound('No genre ' . $genreName . ' found in base');
        }

        $paging = new Paging();
        $perPage = 10;
        $currentPage = $this->getRequest()->getQueryParam('page', 1);
        $paging->setPerPage($perPage);
        $paging->setPage($currentPage);

        $genreId = $row['id'];
        $totalCount = 0;
        $booksIds = $this->getBooksBll()->getIdsByGenreId($genreId, $paging, $totalCount);


        $paging->setTotalItemsCount($totalCount);
        $books = $this->getBooksCollection()->getByIds($booksIds);

        $pages = $paging->getPrettyPages(2, 2, 2);
        $pagerLinks = [];

        foreach ($pages as $page) {
            $pagerLinks[$page] = [
                'link' => '?page=' . $page,
                'id' => $page
            ];
            if ($page == $currentPage) {
                $pagerLinks[$page]['current'] = true;
            }
        }


        return [
            'books' => $books,
            'pageLinks' => $pagerLinks ?? null,
        ];
    }

    public function actionSearch()
    {
        $searchedText = (string) trim($this->getRequest()->getQueryParam('search'));
        $books = [];
        if (mb_strlen($searchedText) >= 3) {
            $sphinx = new SphinxClient();
            $host = $this->configuration()->getSetting('sphinx.host', 'localhost');
            $port = $this->configuration()->getSetting('sphinx.port');
            $sphinx->setServer($host, $port);
            $searchResult = $sphinx->query('*' . $searchedText . '*');
            $booksIds = isset($searchResult['matches']) ? array_keys($searchResult['matches']) : [];
            $paging = new Paging();
            $perPage = 10;
            $currentPage = $this->getRequest()->getQueryParam('page', 1);
            $paging->setPerPage($perPage);
            $paging->setPage($currentPage);
            $paging->setTotalItemsCount(count($booksIds));
            $booksIds = array_slice($booksIds, ($currentPage - 1) * $perPage, $perPage);
            $books = $this->getBooksCollection()->getByIds($booksIds);

            $pages = $paging->getPrettyPages(2, 2, 2);
            $pagerLinks = [];

            foreach ($pages as $page) {
                $pagerLinks[$page] = [
                    'link' => '?page=' . $page . '&search=' . urlencode($searchedText),
                    'id' => $page
                ];
                if ($page == $currentPage) {
                    $pagerLinks[$page]['current'] = true;
                }
            }

        }
        return [
            'books' => $books,
            'pageLinks' => $pagerLinks ?? null,
            'searchText' => $searchedText
        ];
    }

    /**
     * Список книг в слайдере на главной
     * @return array
     */
    public function actionShowBooksOnMain()
    {
        $booksIds = $this->getBooksBll()->getPopularByLastDays(10);
        $books = $this->getBooksCollection()->getByIds($booksIds);
        return [
            'books' => $books
        ];
    }

    public function actionShowAddedBook(array $variables)
    {
        $bookId = (int) $variables['bookId'];
        $book = $this->getBooksCollection()->getById($bookId);
        return [
            'book' => $book
        ];
    }

    /**
     * @return array
     * @todo проверять, что загруженный файл - fb2
     */
    public function actionUploadBookFile()
    {
        $file = $this->getRequest()->getFile('file');
        return $this->uploadBookFile($file);
    }

    /**
     * @param array $file
     * @param MetaData|null $booksMeta
     * @return array
     */
    public function uploadBookFile(array $file, MetaData $booksMeta = null)
    {
        $response = [];
        if (null !== $file) {
            if ($file['size'] && empty($file['error'])) {
                $bookMeta = $booksMeta ?? $this->getFb2Parser()->parse($file['tmp_name']);
                if ($bookMeta->isBook()) {
                    $book = $this->saveBook($file, $bookMeta);
                    $response['success'] = 1;
                    $response['meta'] = $bookMeta;
                    $response['book'] = $book;
                } else {
                    $response['error'] = 'Не удалось распознать книгу в файле';
                }
            }
        }
        return $response;
    }

    public function updateBook(Book $book, MetaData $metaData)
    {
        $book->fillByFB2MetaData($metaData);
        /**
         * привязываем персоналии к книге
         */
        $authors = $this->createBookAuthors($metaData);
        $book->setAuthors($authors);

        $translators = $this->createBookTranslators($metaData);
        $book->setTranslators($translators);

        /**
         * Привязываем жанры к книге
         */
        $genres = $this->createBookGenres($metaData);
        $book->setGenres($genres);
        $book->save();
    }

    private function saveBook(array $file, MetaData $metaData)
    {
        /**
         * добавляем файл
         */
        $fileId = $this->addFile($file);
        /**
         * создаем книгу в бд
         */
        $book = $this->createBook($metaData);
        /**
         * привязываем файл к книге
         */
        $fb2file = new FB2();
        $fb2file->setFileId($fileId);
        $fb2file->setSize((int) $file['size']);
        $book->setFiles([$fb2file]);
        /**
         * привязываем персоналии к книге
         */

        $authors = $this->createBookAuthors($metaData);
        $book->setAuthors($authors);

        $translators = $this->createBookTranslators($metaData);
        $book->setTranslators($translators);

        /**
         * Привязываем жанры к книге
         */
        $genres = $this->createBookGenres($metaData);
        $book->setGenres($genres);
        $book->setHasCover(false);
        $book->save();

        if (!empty($binary = $metaData->getCoverImage('image/jpeg'))) {
            $imagePath = $book->getCoverPath($this->configuration()->getSetting('pathes.upload.books'));
            @mkdir(dirname($imagePath));
            $this->saveJpegCover($binary, $imagePath);
            $book->setHasCover(true);
            $book->save();
        }
        return $book;
    }

    private function saveJpegCover(string $binary, string $filename)
    {
        $im = imagecreatefromstring(base64_decode($binary));
        imagejpeg($im, $filename);
    }

    /**
     * @param MetaData $metaData
     * @return Author[]
     */
    private function createBookAuthors(MetaData $metaData)
    {
        $authors = [];
        foreach ($metaData->getAuthors() as $authorData) {
            $author = new Author();
            $author->setFirstName($authorData['first_name']);
            $author->setLastName($authorData['last_name']);
            $author->setMiddleName($authorData['middle_name']);
            $authorId = $this->getAuthorsBll()->getIdByEntity($author);
            $author->setPersonId($authorId);
            $authors[] = $author;
        }
        return $authors;
    }

    private function createBookTranslators(MetaData $metaData)
    {
        $translators = [];
        foreach ($metaData->getTranslators() as $authorData) {
            $translator = new Translator();
            $translator->setFirstName($authorData['first_name']);
            $translator->setLastName($authorData['last_name']);
            $translator->setMiddleName($authorData['middle_name']);
            $translatorId = $this->getAuthorsBll()->getIdByEntity($translator);
            $translator->setPersonId($translatorId);
            $translators[] = $translator;
        }
        return $translators;
    }

    /**
     * @param MetaData $metaData
     * @return array
     */
    private function createBookGenres(MetaData $metaData)
    {
        $genres = [];
        foreach ($metaData->getGenres() as $genreName) {
            $genre = new Genre();
            $genre->setName($genreName);
            $genreId = $this->getGenresBll()->getIdByEntity($genre);
            $genre->setGenreId($genreId);
            $genres[] = $genre;
        }
        return $genres;
    }

    /**
     * @param MetaData $metaData
     * @return Book
     */
    private function createBook(MetaData $metaData)
    {
        $book = new Book();
        $book->fillByFB2MetaData($metaData);
        return $book;
    }

    /**
     * @param array $file
     * @return bool|string
     */
    private function addFile(array $file)
    {
        $fileId = $this->getFilesBll()->addFile('fb2', (int) $file['size']);
        $fileEntity = new FB2();
        $fileEntity->setFileId($fileId);
        $filePath = $fileEntity->getFilePath(
            $this->configuration()->getSetting('pathes.upload.books')
        );
        $directory = dirname($filePath);
        if (!is_dir($directory)) {
            mkdir(dirname($filePath), 0777, true);
        }
        $moved = rename($file['tmp_name'], $filePath);
        if ($moved) {
            return $fileId;
        }

        return false;
    }

    public function actionShowAddForm()
    {
        return [
            'module' => $this->getModuleKey(),
            'block' => $this->getBlockKey()
        ];
    }
}