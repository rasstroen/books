<?php namespace Books\Module\Books\BLL;

use Books\Core\Database\ConnectionsFactoryTrait;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Injectable\InjectableComponentTrait;
use Books\Core\Entity\Author;


class Authors extends DbTable
{
    use InjectableComponentTrait;
    use ConnectionsFactoryTrait;

    protected function getTableName():string
    {
        return 'person';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    public function setBookLink(int $bookId, int $authorId, int $roleTypeId)
    {
        $values = [
            'book_id' => $bookId,
            'person_id' => $authorId,
            'role_id' => $roleTypeId
        ];
        return $this->getConnectionsFactory()->web->insert('book_persons', $values, array_keys($values));
    }

    /**
     * Ищем в базе автора с такими же ФИО. Возврашаем id если нашли, иначе сохраняем entity и возвращаем id нового автора
     * @param Author $entity
     * @return int
     */
    public function getIdByEntity(Author $entity)
    {
        $authorId = $this->getConnectionsFactory()->web->selectValue(
            'SELECT
              `id`
            FROM
              `' . $entity->getMetaTableName() . '`
            WHERE
                `first_name`= ? AND
                `last_name` = ? AND
                `middle_name` = ?
            ',
            [
                $entity->getFirstName(),
                $entity->getLastName(),
                $entity->getMiddleName()
            ]);
        if (!$authorId) {
            $authorId = $entity->save();
        }
        return $authorId;
    }
}
