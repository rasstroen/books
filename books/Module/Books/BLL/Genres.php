<?php namespace Books\Module\Books\BLL;

use Books\Core\Database\ConnectionsFactoryTrait;
use Plumbus\Core\Module\BLL\Base;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Injectable\InjectableComponentTrait;
use Books\Core\Entity\Genre;

class Genres extends DbTable
{
    use InjectableComponentTrait;
    use ConnectionsFactoryTrait;

    protected function getTableName():string
    {
        return 'genre';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    public function setBookLink(int $bookId, int $genreId)
    {
        $values = [
            'book_id' => $bookId,
            'genre_id' => $genreId
        ];
        return $this->getConnectionsFactory()->web->insert('book_genres', $values, array_keys($values));
    }

    /**
     * @param Genre $entity
     * @return mixed|string
     */
    public function getIdByEntity(Genre $entity)
    {
        $genreId = $this->getConnectionsFactory()->web->selectValue(
            'SELECT
              `id`
            FROM
              `' . $entity->getMetaTableName() . '`
            WHERE
                `name`= ?
            ',
            [
                $entity->getName()
            ]);
        if (!$genreId) {
            $genreId = $entity->save();
        }
        return $genreId;
    }
}

