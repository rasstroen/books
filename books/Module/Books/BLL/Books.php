<?php
namespace Books\Module\Books\BLL;

use Books\Core\Database\ConnectionsFactoryTrait;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Core\Module\BLL\Paging;
use Plumbus\Injectable\InjectableComponentTrait;
use Books\Core\Entity\Basic;
use Books\Core\Entity\Book;

class Books extends DbTable
{
    use InjectableComponentTrait;
    use ConnectionsFactoryTrait;

    protected function getTableName():string
    {
        return 'book';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    /**
     * @param int $genreId
     * @param Paging $paging
     * @param null $totalCount
     * @return array
     */
    public function getIdsByGenreId(int $genreId, Paging $paging, &$totalCount = null)
    {
        $result = $this->getConnectionsFactory()->web->selectColumn('SELECT SQL_CALC_FOUND_ROWS book_id FROM book_genres bg JOIN book b ON b.id=bg.book_id WHERE  genre_id = ? ' . $paging->getLimit(),
            [$genreId]);
        if (null !== $totalCount) {
            $totalCount = $this->getConnectionsFactory()->web->selectValue('SELECT FOUND_ROWS()');
        }
        return $result;
    }

    public function getIdsByPersonId(int $personId, Paging $paging, &$totalCount = null)
    {
        $result = $this->getConnectionsFactory()->web->selectColumn('SELECT SQL_CALC_FOUND_ROWS book_id FROM book_persons bg JOIN book b ON b.id=bg.book_id WHERE person_id = ? ' . $paging->getLimit(),
            [$personId]);
        if (null !== $totalCount) {
            $totalCount = $this->getConnectionsFactory()->web->selectValue('SELECT FOUND_ROWS()');
        }
        return $result;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getPopularByLastDays(int $daysCount = 7)
    {
        return $this->getConnectionsFactory()->web->selectColumn('SELECT `id` FROM book WHERE has_cover=1 ORDER BY RAND() LIMIT ? ',
            [$daysCount]);
    }

    public function getAllByIds(array $ids)
    {
        $query = $this->getQueryByBooksMetaData();
        return $this->getConnectionsFactory()->web->selectAll($query, [$ids]);
    }

    private function getQueryByBooksMetaData()
    {
        $fieldsQuery = [];
        $fields = Book::getMetaFields();
        foreach ($fields as $fieldName => $field) {
            $fieldsQuery[] = "\t" . Book::getMetaTableName() . '.' . $field['field'] . ' as `' . Book::class . '::' . $fieldName . '`';
        }

        $joinQuery = '';
        foreach (Book::getMetaRelations() as $relationClass => $relation) {
            /**
             * @var Basic $relationClass
             */
            foreach ($relationClass::getMetaFields() as $fieldName => $field) {
                $fieldsQuery[] = "\t" . $relationClass::getMetaTableName() . '.' . $field['field'] . ' as `' . $relationClass . '::' . $fieldName . '`';
            }

            if (isset($relation['additional_fields'])) {
                foreach ($relation['additional_fields'] as $additionalFieldName => $additionalField) {
                    $fieldsQuery[] = "\t" . $relation['table'] . '.' . $additionalField['field'] . ' as `' . $relationClass . '::' . $additionalFieldName . '`';
                }
            }


            $joinQuery .= "\n\t" . 'JOIN ' . $relation['table'] . ' ON ' . $relation['table'] . '.' . $relation['field'];
            $joinQuery .= ' = ' . Book::getMetaTableName() . '.' . Book::getIndexField();

            $joinQuery .= "\n\t" . 'JOIN ' . $relationClass::getMetaTableName() . ' ON ' . $relationClass::getMetaTableName() . '.' . $relationClass::getIndexField();
            $joinQuery .= ' = ' . $relation['table'] . '.' . $relation['relation_field'];
        }


        $query = " SELECT \n" . implode(",\n ", $fieldsQuery) . "\n FROM `" . Book::getMetaTableName() . '`';
        $query .= $joinQuery;
        $query .= ' WHERE ' . Book::getMetaTableName() . '.' . Book::getIndexField() . ' IN (?)';
        return $query;
    }
}