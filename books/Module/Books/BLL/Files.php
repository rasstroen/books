<?php namespace Books\Module\Books\BLL;

use Books\Core\Database\ConnectionsFactoryTrait;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Injectable\InjectableComponentTrait;

class Files extends DbTable
{
    use InjectableComponentTrait;
    use ConnectionsFactoryTrait;

    protected function getTableName():string
    {
        return 'file';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    protected function getMetaData():array
    {
        return [];
    }

    public function setBookLink(int $bookId, int $fileId)
    {
        $values = [
            'book_id' => $bookId,
            'file_id' => $fileId,
        ];
        return $this->getConnectionsFactory()->web->insert('book_files', $values, array_keys($values));
    }

    public function addFile(string $extension, int $size)
    {
        $this->getConnectionsFactory()->web->insert(
            $this->getTableName(),
            [
                'file_type' => $extension,
                'size' => $size
            ]
        );
        return $this->getConnectionsFactory()->web->lastInsertId();
    }
}
