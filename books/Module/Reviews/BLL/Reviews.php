<?php  namespace Books\Module\Reviews\BLL;

use Books\Core\Database\ConnectionsFactoryTrait;
use Plumbus\Core\Module\BLL\DbTable;
use Plumbus\Injectable\InjectableComponentTrait;

class Reviews extends DbTable
{
    use ConnectionsFactoryTrait;
    use InjectableComponentTrait;

    protected function getTableName():string
    {
        return 'review';
    }

    protected function getIndexFieldName():string
    {
        return 'id';
    }

    public function getReviews()
    {
        return $this->getConnectionsFactory()->web->selectAll('SELECT * FROM book limit 10');
    }
}