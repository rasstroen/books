<?php namespace Books\Module\Reviews;

use Plumbus\Core\Module\Base;
use Books\Traits\BLL\ReviewsBLLTrait;
use Plumbus\Exception\NotFound;

class Review extends Base
{
    use ReviewsBLLTrait;

    public function actionShowBookReviews(array $variables = null)
    {
        $name = $variables['name'] ?? null;
        if (!$name) {
            throw new NotFound('Illegal book name');
        }

        $transliteratedTitleArray = explode('-', $name);
        $bookId = (int) array_pop($transliteratedTitleArray);

        return ['reviews' => []];
    }
}
