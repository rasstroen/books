<?php namespace Books\Module\User;

use Plumbus\Traits\Classes\User\CurrentUserTrait;

class Profile extends \Plumbus\Core\Module\Base
{
    use CurrentUserTrait;

    public function actionShowProfileLinks()
    {
        return [];
    }
}
