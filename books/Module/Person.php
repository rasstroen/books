<?php namespace Books\Module;

use Books\Core\Entity\Author;
use Books\Traits\BLL\AuthorsBLLTrait;
use Plumbus\Core\Module\Base;
use Plumbus\Exception\NotFound;

class Person extends Base
{
    use AuthorsBLLTrait;

    public function actionShowPerson(array $variables = null)
    {
        $personId = $variables['personId'] ?? null;
        if (!$personId) {
            throw new NotFound('Illegal person id');
        }

        $row = $this->getAuthorsBll()->getById($personId);
        if (!$row) {
            throw new NotFound('No person found in base');
        }

        $person = new Author();

        $person->setFirstName($row['first_name']);
        $person->setMiddleName($row['middle_name']);
        $person->setLastName($row['last_name']);

        return ['person' => $person];
    }
}