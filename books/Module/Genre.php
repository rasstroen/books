<?php namespace Books\Module;

use Books\Traits\BLL\GenresBLLTrait;
use Plumbus\Core\Module\Base;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Exception\NotFound;

class Genre extends Base
{
    use GenresBLLTrait;

    public function actionShowGenre(array $variables = null)
    {
        $genreName = (string) trim($variables['genreName'] ?? '');
        if (!$genreName) {
            throw new NotFound('Illegal genre name');
        }

        $filter = new Filter();
        $filter->setCondition('name', $genreName);
        $row = $this->getGenresBll()->getByFilter($filter);
        if (!empty($row)) {
            $row = reset($row);
        }
        if (!$row) {
            throw new NotFound('No genre ' . $genreName . ' found in base');
        }

        $genre = new \Books\Core\Entity\Genre();

        $genre->setName($row['name']);
        $genre->setTitle($row['title']);


        return ['genre' => $genre];
    }
}