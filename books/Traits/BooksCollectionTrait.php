<?php namespace Books\Traits;

use Books\Module\Books\Collection\Books;

trait BooksCollectionTrait
{
    /**
     * @return Books
     */
    public function getBooksCollection()
    {
        return Books::instance();
    }
}

