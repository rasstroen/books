<?php namespace Books\Traits\BLL;

use Books\Module\Reviews\BLL\Reviews;

trait ReviewsBLLTrait
{
    /**
     * @return Reviews
     */
    public function getReviewsBll()
    {
        return Reviews::instance();
    }
}
