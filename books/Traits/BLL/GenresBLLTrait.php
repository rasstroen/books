<?php namespace Books\Traits\BLL;

use Books\Module\Books\BLL\Genres;

trait GenresBLLTrait
{
    /**
     * @return Genres
     */
    public function getGenresBll()
    {
        return Genres::instance();
    }
}
