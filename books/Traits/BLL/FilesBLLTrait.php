<?php namespace Books\Traits\BLL;

use Books\Module\Books\BLL\Files;

trait FilesBLLTrait
{
    /**
     * @return Files
     */
    public function getFilesBll()
    {
        return Files::instance();
    }
}
