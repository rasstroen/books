<?php namespace Books\Traits\BLL;

use Books\Module\Books\BLL\Books;

trait BooksBLLTrait
{
    /**
     * @return Books
     */
    public function getBooksBll()
    {
        return Books::instance();
    }
}
