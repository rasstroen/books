<?php namespace Books\Traits\BLL;

use Books\Module\Books\BLL\Authors;

trait AuthorsBLLTrait
{
    /**
     * @return Authors
     */
    public function getAuthorsBll()
    {
        return Authors::instance();
    }
}
