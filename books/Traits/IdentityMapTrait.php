<?php namespace Books\Traits;

use Books\Core\IdentityMap;

trait IdentityMapTrait
{
    /**
     * @return IdentityMap
     */
    public function getIdentityMap()
    {
        return IdentityMap::instance();
    }
}

