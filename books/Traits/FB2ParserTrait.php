<?php namespace Books\Traits;

use Books\Core\FB2\Parser;

trait FB2ParserTrait
{
    /**
     * @return Parser
     */
    public function getFb2Parser()
    {
        return Parser::instance();
    }
}

