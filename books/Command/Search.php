<?php namespace Books\Command;

use Books\Traits\BLL\BooksBLLTrait;
use Books\Traits\BooksCollectionTrait;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Core\Module\BLL\Paging;
use Books\Core\Entity\Book;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Search extends Command
{
    use ProjectConfigurationTrait;
    use BooksBLLTrait;
    use BooksCollectionTrait;

     protected function configure()
     {
         $this
             ->setName('search')
             ->setDescription('Generate sphinx search index');
     }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = $this->configuration()->getSetting('pathes.search.indexFolder');
        $output->writeln('creating index in directory: ' . $directory);
        if (!is_dir($directory)) {
            mkdir($directory);
        }

        $page = 1;
        $fileName = $directory . '/books.xml';
        $file = fopen($fileName, 'w');
        fwrite($file, '<?xml version="1.0" encoding="utf-8"?>' . "\n");
        fwrite($file, '<sphinx:docset xmlns:sphinx="http://sphinxsearch.com/">' . "\n");

        $schema = '<sphinx:schema>
            <sphinx:field name="title" attr="string" />
            <sphinx:field name="annotation" attr="text" />
            <sphinx:field name="authors" attr="multi" />
            <sphinx:field name="translators" attr="multi" />
            <sphinx:field name="genres" attr="multi" />
        </sphinx:schema>';
        fwrite($file, $schema);

        do {
            $paging = new Paging();
            $paging->setPage($page);
            $paging->setPerPage(2);
            $filter = new Filter();
            $filter->setPaging($paging);
            $bookRows = $this->getBooksBll()->getByFilter($filter);
            $books = $this->getBooksCollection()->getByIds(array_keys($bookRows));
            foreach ($books as $book) {
                $this->writeBook($book, $file);
            }
            $page += 1;
        } while (count($bookRows));

        fwrite($file, '</sphinx:docset>' . "\n");
        fclose($file);
        $output->writeln('wrote to: ' . $fileName);
    }

    public function writeBook(Book $book, $file)
    {
        fwrite($file, "\n".'<sphinx:document id="' . $book->getId() . '" >');
        fwrite($file, "\n".'<title><![CDATA[' . $book->getTitle() . ']]></title>');
        fwrite($file, "\n".'<annotation><![CDATA[' . $book->getAnnotation() . ']]></annotation>');

        if ($book->getAuthors()) {
            fwrite($file, "\n".'<authors>');
            foreach ($book->getAuthors() as $author) {
                fwrite($file, '<author><![CDATA[');
                fwrite($file, $author->getFirstName() . ' ' . $author->getMiddleName() . ' ' . $author->getLastName());
                fwrite($file, ']]></author>');
            }
            fwrite($file, '</authors>');
        }

        if ($book->getTranslators()) {
            fwrite($file, "\n".'<translators>');
            foreach ($book->getTranslators() as $translator) {
                fwrite($file, '<translator><![CDATA[');
                fwrite($file,
                    $translator->getFirstName() . ' ' . $translator->getMiddleName() . ' ' . $translator->getLastName());
                fwrite($file, ']]></translator>');
            }
            fwrite($file, '</translators>');
        }

        if ($book->getGenres()) {
            fwrite($file, "\n".'<genres>');
            foreach ($book->getGenres() as $genre) {
                fwrite($file, '<genre><![CDATA[');
                fwrite($file,
                    $genre->getTitle());
                fwrite($file, ']]></genre>');
            }
            fwrite($file, '</genres>');
        }
        fwrite($file, "\n".'</sphinx:document>');
    }
}
