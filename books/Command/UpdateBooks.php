<?php
namespace Books\Command;

use Books\Core\Entity\File\FB2;
use Books\Traits\BLL\BooksBLLTrait;
use Books\Traits\BooksCollectionTrait;
use Books\Traits\FB2ParserTrait;
use Plumbus\Core\Module\BLL\Filter;
use Plumbus\Core\Module\BLL\Paging;
use Plumbus\Traits\Core\ProjectConfigurationTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateBooks extends Command
{
    use FB2ParserTrait;
    use BooksCollectionTrait;
    use ProjectConfigurationTrait;
    use BooksBLLTrait;

    protected function configure()
    {
        $this
            ->setName('updateBooks')
            ->addOption('bookId', 'id', InputOption::VALUE_OPTIONAL, '', '')
            ->setDescription('Parse fb2 files again');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bookId = $input->getOption('bookId');
        if ($bookId) {
            $bookIds = [$bookId];
            $this->processBooksChunk($bookIds, $output);
        } else {
            $filter = new Filter();
            $paging = new Paging();

            $paging->setPerPage(10);
            $page = 1;
            do {
                $paging->setPage($page++);
                $filter->setPaging($paging);
                $rows = $this->getBooksBll()->getByFilter($filter);
                $bookIds = array_keys($rows);
                $this->processBooksChunk($bookIds, $output);
            } while (count($bookIds));
        }
    }

    private function processBooksChunk(array $bookIds, OutputInterface $output)
    {
        $output->writeln('processing books' . implode(',', $bookIds));
        $books = $this->getBooksCollection()->getByIds($bookIds);
        foreach ($books as $book) {
            $output->writeln($book->getTitle() . ', id:' . $book->getId());
            $files = $book->getFiles();
            foreach ($files as $file) {
                if ($file instanceof FB2) {
                    $path = $file->getFilePath($this->configuration()->getSetting('pathes.upload.books'));
                    $output->writeln('got fb2 file ' . $path);
                    $metaData = $this->getFb2Parser()->parse($path);
                    $booksModule = new \Books\Module\Books\Books();
                    $booksModule->updateBook($book, $metaData);
                }
            }
        }

    }
}
