<?php
namespace Books\Command;

use Books\Traits\FB2ParserTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Parse extends Command
{
    use FB2ParserTrait;

    protected function configure()
    {
        $this
            ->setName('parse')
            ->addOption('dir', 'd', InputOption::VALUE_OPTIONAL, '', '/tmp')
            ->setDescription('Parse fb2 files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Parsing.');


        $output->writeln('parsing directory with fb2 files');
        $directory = $input->getOption('dir');
        $output->writeln('processing directory ' . $directory);

        $directory = new \RecursiveDirectoryIterator($directory);
        $iterator = new \RecursiveIteratorIterator($directory);
        $regexIterator = new \RegexIterator($iterator, '/^.+\.fb2$/i', \RecursiveRegexIterator::GET_MATCH);
        $filenames = [];
        foreach ($regexIterator as $fileName) {
            $filenames[reset($fileName)] = true;
        }
        $output->writeln('total ' . count($filenames) . ' fb2 files found');

        $successCount = 0;
        $failsCount = 0;
        foreach (array_keys($filenames) as $index => $filename) {
            if ($this->parseFb2($index, $filename, $output)) {
                $successCount++;
            } else {
                $failsCount++;
            }
        }
        $output->writeln('total success' . $successCount . ', fails: ' . $failsCount);
    }

    private function parseFb2($index, $filename, OutputInterface $output)
    {
        $bookMeta = $this->getFb2Parser()->parse($filename);
        $genres = $bookMeta->getGenres();
        $allowedGenres = [
            'детская фантастика',
            'дет',
            'детская'
        ];
        $needParse = false;
        foreach ($genres as $genre) {
            if (strpos($genre, 'child') === 0) {
                $needParse = true;
            } elseif (in_array($genre, $allowedGenres)) {
                $needParse = true;
            }
        }
        if ($needParse) {
            $output->writeln('genre ok ' . $index);
            $booksModule = new \Books\Module\Books\Books();
            $file = ['size' => filesize($filename), 'tmp_name' => $filename];
            $response = $booksModule->uploadBookFile($file);
            if (!empty($response['success'])) {
                $output->writeln($response['book']->getTitle());
                return true;
            }
        } else {
            $output->writeln('no genres ' . $index . ' ' . implode(', ', $bookMeta->getGenres()));
            unlink($filename);
        }
        return false;
    }
}
