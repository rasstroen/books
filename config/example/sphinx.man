1. yum install postgresql-libs unixODBC
2. качаем sphinx-2.2.10-1.rhel6.x86_64.rpm отсюда http://sphinxsearch.com/downloads/release/
3. rpm -Uhv sphinx-2.2.10-1.rhel6.x86_64.rpm
4. mkdir /home/sites/books/search &&  chmod 777 /home/sites/books/search/
5. mv /home/sites/books/config/sphinx.conf /etc/sphinx/sphinx.conf
6. cd /home/sites/books/ && php books/bin/run.php search rotate
7. indexer --rotate --all
8. service searchd restart

Для обновления индекса (потом в крон запихаем)
1. cd /home/sites/books/ && php books/bin/run.php search rotate
2. indexer --rotate --all