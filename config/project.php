<?php
$config = require_once __DIR__ . '/main.php';

if (is_readable(__DIR__ . '/console.php')) {
    $config = array_merge($config, require_once __DIR__ . '/console.php');
}

$config['pathes']['upload']['books'] = __DIR__ . '/../upload';
$config['pathes']['search']['indexFolder'] = __DIR__ . '/../search';
$config['rootPath'] = __DIR__ . '/../';
$config['sphinx'] = [
    'host' => env('SPHINX_HOST', '127.0.0.1'),
    'port' => env('SPHINX_PORT', '9312'),
];

$config['social'] = [
    'networks' => [
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_VKONTAKTE => [
            'enabled' => true,
            'app_id' => env('VK_APP_ID'),
            'app_secret' => env('VK_APP_SECRET'),
            'connect_url' => 'http://' . env('DOMAIN') . '/connect/vk'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_FACEBOOK => [
            'enabled' => true,
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'connect_url' => 'http://' . env('DOMAIN') . '/connect/fb'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_ODNOKLASSNIKI => [
            'enabled' => true,
            'app_id' => env('OK_APP_ID'),
            'app_secret' => env('OK_APP_SECRET'),
            'app_public' => env('OK_APP_PUBLIC'),
            'extended_access' => true,
            'connect_url' => 'http://' . env('DOMAIN') . '/connect/ok',
        ],
    ]
];

$config['languages'] = [
    'aa' => 1,
    'ab' => 2,
    'ae' => 3,
    'af' => 4,
    'ak' => 5,
    'am' => 6,
    'an' => 7,
    'ar' => 8,
    'as' => 9,
    'av' => 10,
    'ay' => 11,
    'az' => 12,
    'ba' => 13,
    'be' => 14,
    'bg' => 15,
    'bh' => 16,
    'bi' => 17,
    'bm' => 18,
    'bn' => 19,
    'bo' => 20,
    'br' => 21,
    'bs' => 22,
    'ca' => 23,
    'ce' => 24,
    'ch' => 25,
    'co' => 26,
    'cr' => 27,
    'cs' => 28,
    'cu' => 29,
    'cv' => 30,
    'cy' => 31,
    'da' => 32,
    'de' => 33,
    'dv' => 34,
    'dz' => 35,
    'ee' => 36,
    'el' => 37,
    'eN' => 38,
    'en' => 38,
    'eo' => 39,
    'es' => 40,
    'et' => 41,
    'eu' => 42,
    'fa' => 43,
    'ff' => 44,
    'fi' => 45,
    'fj' => 46,
    'fo' => 47,
    'fr' => 48,
    'fy' => 49,
    'ga' => 50,
    'gd' => 51,
    'gl' => 52,
    'gn' => 53,
    'gu' => 54,
    'gv' => 55,
    'ha' => 56,
    'he' => 57,
    'hi' => 58,
    'ho' => 59,
    'hr' => 60,
    'ht' => 61,
    'hu' => 62,
    'hy' => 63,
    'hz' => 64,
    'ia' => 65,
    'id' => 66,
    'ie' => 67,
    'ig' => 68,
    'ii' => 69,
    'ik' => 70,
    'io' => 71,
    'is' => 72,
    'it' => 73,
    'iu' => 74,
    'ja' => 75,
    'jv' => 76,
    'ka' => 77,
    'kg' => 78,
    'ki' => 79,
    'kj' => 80,
    'kk' => 81,
    'kl' => 82,
    'km' => 83,
    'kn' => 84,
    'ko' => 85,
    'kr' => 86,
    'ks' => 87,
    'ku' => 88,
    'kv' => 89,
    'kw' => 90,
    'ky' => 91,
    'la' => 92,
    'lb' => 93,
    'lg' => 94,
    'li' => 95,
    'ln' => 96,
    'lo' => 97,
    'lt' => 98,
    'lu' => 99,
    'lv' => 100,
    'mg' => 101,
    'mh' => 102,
    'mi' => 103,
    'mk' => 104,
    'ml' => 105,
    'mn' => 106,
    'mr' => 107,
    'ms' => 108,
    'mt' => 109,
    'my' => 110,
    'na' => 111,
    'nb' => 112,
    'nd' => 113,
    'ne' => 114,
    'ng' => 115,
    'nl' => 116,
    'nn' => 117,
    'no' => 118,
    'nr' => 119,
    'nv' => 120,
    'ny' => 121,
    'oc' => 122,
    'oj' => 123,
    'om' => 124,
    'or' => 125,
    'os' => 126,
    'pa' => 127,
    'pi' => 128,
    'pl' => 129,
    'ps' => 130,
    'pt' => 131,
    'qu' => 132,
    'rm' => 133,
    'rn' => 134,
    'ro' => 135,
    'ru' => 136,
    'rw' => 137,
    'sa' => 138,
    'sc' => 139,
    'sd' => 140,
    'se' => 141,
    'sg' => 142,
    'si' => 143,
    'sk' => 144,
    'sl' => 145,
    'sm' => 146,
    'sn' => 147,
    'so' => 148,
    'sq' => 149,
    'sr' => 150,
    'ss' => 151,
    'st' => 152,
    'su' => 153,
    'sv' => 154,
    'sw' => 155,
    'ta' => 156,
    'te' => 157,
    'tg' => 158,
    'th' => 159,
    'ti' => 160,
    'tk' => 161,
    'tl' => 162,
    'tn' => 163,
    'to' => 164,
    'tr' => 165,
    'ts' => 166,
    'tt' => 167,
    'tw' => 168,
    'ty' => 169,
    'ug' => 170,
    'uk' => 171,
    'ur' => 172,
    'uz' => 173,
    've' => 174,
    'vi' => 175,
    'vo' => 176,
    'wa' => 177,
    'wo' => 178,
    'xh' => 179,
    'yi' => 180,
    'yo' => 181,
    'za' => 182,
    'zh' => 183,
    'zu' => 184,
    'xx' => 185
];


$config['languages_id'] = [
    1 => 'aa',
    2 => 'ab',
    3 => 'ae',
    4 => 'af',
    5 => 'ak',
    6 => 'am',
    7 => 'an',
    8 => 'ar',
    9 => 'as',
    10 => 'av',
    11 => 'ay',
    12 => 'az',
    13 => 'ba',
    14 => 'be',
    15 => 'bg',
    16 => 'bh',
    17 => 'bi',
    18 => 'bm',
    19 => 'bn',
    20 => 'bo',
    21 => 'br',
    22 => 'bs',
    23 => 'ca',
    24 => 'ce',
    25 => 'ch',
    26 => 'co',
    27 => 'cr',
    28 => 'cs',
    29 => 'cu',
    30 => 'cv',
    31 => 'cy',
    32 => 'da',
    33 => 'de',
    34 => 'dv',
    35 => 'dz',
    36 => 'ee',
    37 => 'el',
    38 => 'en',
    39 => 'eo',
    40 => 'es',
    41 => 'et',
    42 => 'eu',
    43 => 'fa',
    44 => 'ff',
    45 => 'fi',
    46 => 'fj',
    47 => 'fo',
    48 => 'fr',
    49 => 'fy',
    50 => 'ga',
    51 => 'gd',
    52 => 'gl',
    53 => 'gn',
    54 => 'gu',
    55 => 'gv',
    56 => 'ha',
    57 => 'he',
    58 => 'hi',
    59 => 'ho',
    60 => 'hr',
    61 => 'ht',
    62 => 'hu',
    63 => 'hy',
    64 => 'hz',
    65 => 'ia',
    66 => 'id',
    67 => 'ie',
    68 => 'ig',
    69 => 'ii',
    70 => 'ik',
    71 => 'io',
    72 => 'is',
    73 => 'it',
    74 => 'iu',
    75 => 'ja',
    76 => 'jv',
    77 => 'ka',
    78 => 'kg',
    79 => 'ki',
    80 => 'kj',
    81 => 'kk',
    82 => 'kl',
    83 => 'km',
    84 => 'kn',
    85 => 'ko',
    86 => 'kr',
    87 => 'ks',
    88 => 'ku',
    89 => 'kv',
    90 => 'kw',
    91 => 'ky',
    92 => 'la',
    93 => 'lb',
    94 => 'lg',
    95 => 'li',
    96 => 'ln',
    97 => 'lo',
    98 => 'lt',
    99 => 'lu',
    100 => 'lv',
    101 => 'mg',
    102 => 'mh',
    103 => 'mi',
    104 => 'mk',
    105 => 'ml',
    106 => 'mn',
    107 => 'mr',
    108 => 'ms',
    109 => 'mt',
    110 => 'my',
    111 => 'na',
    112 => 'nb',
    113 => 'nd',
    114 => 'ne',
    115 => 'ng',
    116 => 'nl',
    117 => 'nn',
    118 => 'no',
    119 => 'nr',
    120 => 'nv',
    121 => 'ny',
    122 => 'oc',
    123 => 'oj',
    124 => 'om',
    125 => 'or',
    126 => 'os',
    127 => 'pa',
    128 => 'pi',
    129 => 'pl',
    130 => 'ps',
    131 => 'pt',
    132 => 'qu',
    133 => 'rm',
    134 => 'rn',
    135 => 'ro',
    136 => 'ru',
    137 => 'rw',
    138 => 'sa',
    139 => 'sc',
    140 => 'sd',
    141 => 'se',
    142 => 'sg',
    143 => 'si',
    144 => 'sk',
    145 => 'sl',
    146 => 'sm',
    147 => 'sn',
    148 => 'so',
    149 => 'sq',
    150 => 'sr',
    151 => 'ss',
    152 => 'st',
    153 => 'su',
    154 => 'sv',
    155 => 'sw',
    156 => 'ta',
    157 => 'te',
    158 => 'tg',
    159 => 'th',
    160 => 'ti',
    161 => 'tk',
    162 => 'tl',
    163 => 'tn',
    164 => 'to',
    165 => 'tr',
    166 => 'ts',
    167 => 'tt',
    168 => 'tw',
    169 => 'ty',
    170 => 'ug',
    171 => 'uk',
    172 => 'ur',
    173 => 'uz',
    174 => 've',
    175 => 'vi',
    176 => 'vo',
    177 => 'wa',
    178 => 'wo',
    179 => 'xh',
    180 => 'yi',
    181 => 'yo',
    182 => 'za',
    183 => 'zh',
    184 => 'zu',
    185 => 'xx',
];
$config['languages_translations'] = [
    'aa' => 'афар',
    'ab' => 'абхазский',
    'ae' => 'авестийский',
    'af' => 'африканас',
    'ak' => 'акан',
    'am' => 'амхарский',
    'an' => 'арагонский',
    'ar' => 'арабский',
    'as' => 'ассамский',
    'av' => 'аварский',
    'ay' => 'аймара',
    'az' => 'азербайджанский',
    'ba' => 'башкирский',
    'be' => 'белорусский',
    'bg' => 'болгарский',
    'bh' => 'бихари; бходжпури; магахи; майтхили',
    'bi' => 'бислама',
    'bm' => 'бамбара',
    'bn' => 'бенгали',
    'bo' => 'тибетский',
    'br' => 'бретонский',
    'bs' => 'боснийский',
    'ca' => 'каталанский; валенсийский',
    'ce' => 'чеченский',
    'ch' => 'чаморро',
    'co' => 'корсиканский',
    'cr' => 'кри',
    'cs' => 'чешский',
    'cu' => 'церковнославянский; старославянский; староболгарский',
    'cv' => 'чувашский',
    'cy' => 'валлийский',
    'da' => 'датский',
    'de' => 'немецкий',
    'dv' => 'дивехи; мальдивский',
    'dz' => 'дзонг-кэ',
    'ee' => 'эве',
    'el' => 'греческий',
    'en' => 'английский',
    'eo' => 'эсперанто',
    'es' => 'испанский; кастильский',
    'et' => 'эстонский',
    'eu' => 'баскский',
    'fa' => 'персидский',
    'ff' => 'фулах',
    'fi' => 'финский',
    'fj' => 'фиджи',
    'fo' => 'фарерский',
    'fr' => 'французский',
    'fy' => 'фризский, западный',
    'ga' => 'ирландский',
    'gd' => 'гэльский; гаэльский; шотландский гэльский',
    'gl' => 'галисийский',
    'gn' => 'гварани',
    'gu' => 'гуджарати',
    'gv' => 'мэнский',
    'ha' => 'хауса',
    'he' => 'иврит',
    'hi' => 'хинди',
    'ho' => 'хиримоту',
    'hr' => 'хорватский',
    'ht' => 'гаитянский; гаитянский креольский',
    'hu' => 'венгерский',
    'hy' => 'армянский',
    'hz' => 'гереро',
    'ia' => 'интерлингва',
    'id' => 'индонезийский',
    'ie' => 'окциденталь; интерлингве',
    'ig' => 'игбо',
    'ii' => 'сычуаньский',
    'ik' => 'инулиак',
    'io' => 'идо',
    'is' => 'исландский',
    'it' => 'итальянский',
    'iu' => 'инуктитут',
    'ja' => 'японский',
    'jv' => 'яванский',
    'ka' => 'грузинский',
    'kg' => 'конкани',
    'ki' => 'кикуйю',
    'kj' => 'киньяма',
    'kk' => 'казахский',
    'kl' => 'эскимосский; гренландский',
    'km' => 'кхмер',
    'kn' => 'каннада',
    'ko' => 'корейский',
    'kr' => 'канури',
    'ks' => 'кашмири',
    'ku' => 'курдский',
    'kv' => 'коми',
    'kw' => 'корнский',
    'ky' => 'киргизский; кыргызский',
    'la' => 'латинский',
    'lb' => 'люксембургский',
    'lg' => 'ганда',
    'li' => 'лимбургский',
    'ln' => 'лингала',
    'lo' => 'лаосский',
    'lt' => 'литовский',
    'lu' => 'луба-катанга',
    'lv' => 'латышский',
    'mg' => 'малагасийский',
    'mh' => 'маршальский',
    'mi' => 'маори',
    'mk' => 'македонский',
    'ml' => 'малаялам',
    'mn' => 'монгольский',
    'mr' => 'маратхи',
    'ms' => 'малайский',
    'mt' => 'мальтийский',
    'my' => 'бирманский',
    'na' => 'науру',
    'nb' => 'норвежский букмол',
    'nd' => 'ндебеле, северный',
    'ne' => 'непальский',
    'ng' => 'ндунга',
    'nl' => 'нидерландский; голландский; фламандский',
    'nn' => 'норвежский нюнорск',
    'no' => 'норвежский',
    'nr' => 'ндебеле, южный',
    'nv' => 'навахо',
    'ny' => 'ньянджа',
    'oc' => 'окситанский',
    'oj' => 'оджибва',
    'om' => 'оромо',
    'or' => 'ория',
    'os' => 'осетинский',
    'pa' => 'панджаби',
    'pi' => 'пали',
    'pl' => 'польский',
    'ps' => 'пушту',
    'pt' => 'португальский',
    'qu' => 'кечуа',
    'rm' => 'романский; ретороманский',
    'rn' => 'кирунди',
    'ro' => 'румынский; молдавский',
    'ru' => 'русский',
    'rw' => 'киньяруанда',
    'sa' => 'санскрит',
    'sc' => 'сардинский',
    'sd' => 'синдхи',
    'se' => 'саамский, северный',
    'sg' => 'санго',
    'si' => 'сингальский',
    'sk' => 'словацкий',
    'sl' => 'словенский',
    'sm' => 'самоанский',
    'sn' => 'шона',
    'so' => 'сомали',
    'sq' => 'албанский',
    'sr' => 'сербский',
    'ss' => 'свази',
    'st' => 'сото, северный',
    'su' => 'сунданский',
    'sv' => 'шведский',
    'sw' => 'суахили',
    'ta' => 'тамильский',
    'te' => 'телугу',
    'tg' => 'таджикский',
    'th' => 'таиландский',
    'ti' => 'тигринья',
    'tk' => 'туркменский',
    'tl' => 'тагалог',
    'tn' => 'тсвана',
    'to' => 'тонга',
    'tr' => 'турецкий',
    'ts' => 'тсонга',
    'tt' => 'татарский',
    'tw' => 'тви',
    'ty' => 'таитянский',
    'ug' => 'уйгурский',
    'uk' => 'украинский',
    'ur' => 'урду',
    'uz' => 'узбекский',
    've' => 'венда',
    'vi' => 'вьетнамский',
    'vo' => 'волапюк',
    'wa' => 'валлонский',
    'wo' => 'волоф',
    'xh' => 'коса',
    'yi' => 'идиш',
    'yo' => 'йоруба',
    'za' => 'чжуань',
    'zh' => 'китайский',
    'zu' => 'зулу',
];

return $config;