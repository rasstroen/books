<?php
/**
 * Плашка авторизации на каждой странице
 */
$modules['auth_plank'] = [
    'class' => Books\Module\User\Authorization::class,
    'template' => 'user/topPlank.twig',
    'action' => 'showAuthorization',
    'ssi' => true,
];

/**
 * Страница, куда попадает пользователь при авторизации через соцсети
 */
$modules['connect'] = [
    'class' => \Project\Module\Connect\Connect::class,
    'template' => 'user/connect.twig',
    'action' => 'showConnect',
];

/**
 * Верхнее меню
 */
$modules['top_menu'] = [
    'class' => Books\Module\Html\Text::class,
    'template' => 'html/text/topMenu.twig',
    'action' => 'showTopMenu'
];

/**
 * Футер
 */
$modules['footer'] = [
    'class' => Books\Module\Html\Text::class,
    'template' => 'html/text/footer.twig',
    'action' => 'showFooter'
];


/**
 * Форма на странице логина
 */
$modules['login'] = [
    'class' => Books\Module\User\Authorization::class,
    'template' => 'user/login.twig',
    'action' => 'showAuthorization'
];

/**
 * текст на главной
 */
$modules['text_on_main'] = [
    'class' => Books\Module\Html\Text::class,
    'template' => 'html/text/main.twig',
    'action' => 'showTextOnMain',
    'cache' => 0,
    'css' => [
        '/static/css/module/html/text/main.css'
    ]
];

/**
 * Просмотр книги
 */
$modules['book'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/book.twig',
    'action' => 'showBook',
];

/**
 * Жанр
 */
$modules['genre'] = [
    'class' => Books\Module\Genre::class,
    'template' => 'genre.twig',
    'action' => 'showGenre',
];

/**
 * Книги жанра
 */
$modules['genre_books'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/genre_books.twig',
    'action' => 'showGenreBooks',
];

/**
 * Просмотр страницы автора
 */
$modules['person'] = [
    'class' => Books\Module\Person::class,
    'template' => 'person.twig',
    'action' => 'showPerson',
];

/**
 * Книги автора
 */
$modules['person_books'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/person_books.twig',
    'action' => 'showPersonBooks',
];

$modules['reviews'] = [
    'class' => Books\Module\Reviews\Review::class,
    'template' => 'review/list.twig',
    'action' => 'showBookReviews',
];

/**
 * Чтение книги
 */
$modules['book_read'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/read.twig',
    'action' => 'readBook',
];

/**
 * Поиск
 */
$modules['search'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/search.twig',
    'action' => 'search',
];

/**
 * Список книг на главной
 */
$modules['books_on_main'] = [
    'class' => Books\Module\Books\Books::class,
    'template' => 'books/onmain.twig',
    'action' => 'showBooksOnMain',
    'cache' => 0,
];
/**
 * Текст на странице моего профиля
 */
$modules['text_on_my_profile'] = [
    'class' => Books\Module\Html\Text::class,
    'template' => 'html/text/myProfile.twig',
    'action' => 'showTextOnMyProfile',
    'cache' => 0,
    'css' => [
        '/static/css/module/html/text/main.css'
    ]
];

/**
 * Текст на странице моего профиля
 */
$modules['links_on_my_profile'] = [
    'class' => Books\Module\User\Profile::class,
    'template' => 'user/profileLinks.twig',
    'action' => 'showProfileLinks',
    'cache' => 0,
    'css' => [
        '/static/css/module/user/profile/links.css'
    ]
];

/**
 * Форма добавления книги
 */
$modules['add_book_form'] = [
    'class' => \Books\Module\Books\Books::class,
    'template' => 'books/addForm.twig',
    'action' => 'showAddForm',
    'cache' => 0,
    'css' => [
        '/static/css/module/books/addForm.css'
    ]
];

/**
 * Только что добавленная пользователем книга - просмотр получившейся книги
 */
$modules['just_added_book'] = [
    'class' => \Books\Module\Books\Books::class,
    'template' => 'books/justAdded.twig',
    'action' => 'showAddedBook',
    'cache' => 0,
    'css' => [
        '/static/css/module/books/justAdded.css'
    ]
];

return $modules;