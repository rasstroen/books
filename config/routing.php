<?php
return [
    \Plumbus\Router::DEFAULT_REQUEST_TYPE_NAME => [
        '' => 'index',
        'book' => [
            'read' => [
                '%s=name' => [
                    '' => 'book_read',
                ]
            ],
            '%s=name' => [
                '' => 'book',
            ]
        ],
        'person' => [
            '%d=personId' => 'person',
        ],
        'genre' => [
            '%s=genreName' => 'genre',
        ],
        'connect' => [
            '%s=network' => 'connect',
        ],
        'search' => [
            '' => 'search',
        ],
        'my' => [
            '' => 'my_profile',
            'books' => [
                'add' => 'add_book',
                '%d=bookId' => [
                    '' => 'added_book'
                ]
            ]
        ]
    ],
];
