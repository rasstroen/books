<?php
$logger = \Plumbus\Core\Logger\Logger::instance();
/**
 * @var \Plumbus\Core\Logger\Logger $logger
 */
$interval = $logger->start('config:pages');

$modules = require_once 'modules.php';


$css['olympia/twoColumns'] = [
    '/static/css/project.css',
    '/static/css/olympia/css/reset.css',
    '/static/css/olympia/css/text.css',
    'http://fonts.googleapis.com/css?family=Questrial|PT+Sans:400,700|Muli',
    '/static/css/olympia/css/prettyPhoto.css',
    '/static/css/olympia/css/jquery.jqzoom.css',
    '/static/css/olympia/css/icons/icons.css',
    '/static/css/olympia/css/custom.css',
    '/static/css/olympia/css/responsive.css',
    '/static/css/olympia/css/styles/red.css'
];

$js['olympia/twoColumns'] = [
    '/static/js/olympia/js/jquery-1.7.1.min.js',
    '/static/js/olympia/js/jquery.easing.1.3.js',
    '/static/js/olympia/js/jquery-ui-1.10.3.min.js',
    '/static/js/olympia/js/jquery.flexslider.js',
    '/static/js/olympia/js/idangerous.swiper-1.9.min.js',
    '/static/js/olympia/js/jquery.sharrre-1.3.4.min.js',
    '/static/js/olympia/js/kenburns.js',
    '/static/js/olympia/js/twitter/jquery.tweet.js',
    '/static/js/olympia/js/jquery.fitvids.min.js',
    '/static/js/olympia/js/jquery.jqzoom-core-pack.js',
    '/static/js/olympia/js/jquery.prettyPhoto.js',
    '/static/js/olympia/js/custom.js',
];

$css['olympia/oneColumn'] = [
    '/static/css/project.css',
    '/static/css/olympia/css/reset.css',
    '/static/css/olympia/css/text.css',
    'http://fonts.googleapis.com/css?family=Questrial|PT+Sans:400,700|Muli',
    '/static/css/olympia/css/prettyPhoto.css',
    '/static/css/olympia/css/jquery.jqzoom.css',
    '/static/css/olympia/css/icons/icons.css',
    '/static/css/olympia/css/custom.css',
    '/static/css/olympia/css/responsive.css',
    '/static/css/olympia/css/styles/red.css'
];

$js['olympia/oneColumn'] = [
    '/static/js/olympia/js/jquery-1.7.1.min.js',
    '/static/js/olympia/js/jquery.easing.1.3.js',
    '/static/js/olympia/js/jquery-ui-1.10.3.min.js',
    '/static/js/olympia/js/jquery.flexslider.js',
    '/static/js/olympia/js/idangerous.swiper-1.9.min.js',
    '/static/js/olympia/js/jquery.sharrre-1.3.4.min.js',
    '/static/js/olympia/js/kenburns.js',
    '/static/js/olympia/js/twitter/jquery.tweet.js',
    '/static/js/olympia/js/jquery.fitvids.min.js',
    '/static/js/olympia/js/jquery.jqzoom-core-pack.js',
    '/static/js/olympia/js/jquery.prettyPhoto.js',
    '/static/js/olympia/js/custom.js',
];

$css['olympia/index'] = [
    '/static/css/project.css',
    '/static/css/olympia/css/reset.css',
    '/static/css/olympia/css/text.css',
    'http://fonts.googleapis.com/css?family=Questrial|PT+Sans:400,700|Muli',
    '/static/css/olympia/css/prettyPhoto.css',
    '/static/css/olympia/css/jquery.jqzoom.css',
    '/static/css/olympia/css/icons/icons.css',
    '/static/css/olympia/css/custom.css',
    '/static/css/olympia/css/responsive.css',
    '/static/css/olympia/css/styles/red.css',
];

$js['olympia/index'] = [
    '/static/js/olympia/js/jquery-1.7.1.min.js',
    '/static/js/olympia/js/jquery.easing.1.3.js',
    '/static/js/olympia/js/jquery-ui-1.10.3.min.js',
    '/static/js/olympia/js/jquery.flexslider.js',
    '/static/js/olympia/js/idangerous.swiper-1.9.min.js',
    '/static/js/olympia/js/jquery.sharrre-1.3.4.min.js',
    '/static/js/olympia/js/kenburns.js',
    '/static/js/olympia/js/twitter/jquery.tweet.js',
    '/static/js/olympia/js/jquery.fitvids.min.js',
    '/static/js/olympia/js/jquery.jqzoom-core-pack.js',
    '/static/js/olympia/js/jquery.prettyPhoto.js',
    '/static/js/olympia/js/custom.js',
];

$css['olympia/login'] = [
    '/static/css/project.css',
    '/static/css/olympia/css/reset.css',
    '/static/css/olympia/css/text.css',
    'http://fonts.googleapis.com/css?family=Questrial|PT+Sans:400,700|Muli',
    '/static/css/olympia/css/prettyPhoto.css',
    '/static/css/olympia/css/jquery.jqzoom.css',
    '/static/css/olympia/css/icons/icons.css',
    '/static/css/olympia/css/custom.css',
    '/static/css/olympia/css/responsive.css',
    '/static/css/olympia/css/styles/red.css',
];

$js['olympia/login'] = [
    '/static/js/olympia/js/jquery-1.7.1.min.js',
    '/static/js/olympia/js/jquery.easing.1.3.js',
    '/static/js/olympia/js/jquery-ui-1.10.3.min.js',
    '/static/js/olympia/js/jquery.flexslider.js',
    '/static/js/olympia/js/idangerous.swiper-1.9.min.js',
    '/static/js/olympia/js/jquery.sharrre-1.3.4.min.js',
    '/static/js/olympia/js/kenburns.js',
    '/static/js/olympia/js/twitter/jquery.tweet.js',
    '/static/js/olympia/js/jquery.fitvids.min.js',
    '/static/js/olympia/js/jquery.jqzoom-core-pack.js',
    '/static/js/olympia/js/jquery.prettyPhoto.js',
    '/static/js/olympia/js/custom.js',
];

/**
 * Главная страница
 */
$pages['index'] = new \Plumbus\Core\Controller\Page\SimplePage([
    'layout' => 'olympia/index',
    'cache' => 0,
    'css' => $css['olympia/index'],
    'js' => $js['olympia/index'],
    'title' => 'Книжный домик',
    'description' => 'Книжный домик',
    'blocks' => [
        'top' => [
            'auth' => $modules['auth_plank'],
        ],
        'head' => [
            'menu' => $modules['top_menu'],
        ],
        'content' => [
            'text' => $modules['text_on_main'],
            'books_on_main' => $modules['books_on_main'],
        ],
        'footer' => [
            'footer' => $modules['footer'],
        ],
    ],
]);

/**
 * Лендинг при авторизации через соцсеть
 */
$pages['connect'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'layout' => 'olympia/twoColumns',
        'title' => 'Авторизация',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'connect' => $modules['connect'],
            ],
            'sidebar' => [

            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница книги
 */
$pages['book'] = new \Books\Core\Page\BookPage(
    [
        'layout' => 'olympia/twoColumns',
        'title' => 'Книга',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'book' => $modules['book'],
                'reviews' => $modules['reviews'],
            ],
            'sidebar' => [

            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница автора/переводчика
 */
$pages['person'] = new \Books\Core\Page\PersonPage(
    [
        'layout' => 'olympia/twoColumns',
        'title' => 'Автор',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'person_books' => $modules['person_books'],
            ],
            'sidebar' => [
                'person' => $modules['person'],
            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница жанра
 */
$pages['genre'] = new \Books\Core\Page\GenrePage(
    [
        'layout' => 'olympia/twoColumns',
        'title' => 'Жанр',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'genre_books' => $modules['genre_books'],
            ],
            'sidebar' => [
                'genre' => $modules['genre'],
            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница чтения книги
 */
$pages['book_read'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'layout' => 'olympia/oneColumn',
        'title' => 'Книга',
        'css' => $css['olympia/oneColumn'],
        'js' => $js['olympia/oneColumn'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'book' => $modules['book_read'],
            ],
            'sidebar' => [

            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);
/**
 *  поиск
 */
$pages['search'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'layout' => 'olympia/twoColumns',
        'title' => 'Поиск',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'search' => $modules['search'],
            ],
            'sidebar' => [

            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница моего профиля
 */
$pages['my_profile'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'role' => \Plumbus\Core\User\CurrentUser::ROLE_USER,
        'layout' => 'olympia/twoColumns',
        'title' => 'Профиль пользователя',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'text' => $modules['text_on_my_profile'],
            ],
            'sidebar' => [
                'links' => $modules['links_on_my_profile'],
            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

/**
 * Страница добавления книги
 */
$pages['add_book'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'role' => \Plumbus\Core\User\CurrentUser::ROLE_USER,
        'layout' => 'olympia/twoColumns',
        'title' => 'Добавление книги',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'add_form' => $modules['add_book_form'],
            ],
            'sidebar' => [
                'links' => $modules['links_on_my_profile'],
            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

$pages['added_book'] = new \Plumbus\Core\Controller\Page\SimplePage(
    [
        'role' => \Plumbus\Core\User\CurrentUser::ROLE_USER,
        'layout' => 'olympia/twoColumns',
        'cache' => 0,
        'title' => 'Добавленная книга',
        'css' => $css['olympia/twoColumns'],
        'js' => $js['olympia/twoColumns'],
        'blocks' => [
            'top' => [
                'auth' => $modules['auth_plank'],
            ],
            'head' => [
                'menu' => $modules['top_menu'],
            ],
            'content' => [
                'book' => $modules['just_added_book'],
            ],
            'sidebar' => [
                'links' => $modules['links_on_my_profile'],
            ],
            'footer' => [
                'footer' => $modules['footer'],
            ],
        ]
    ]
);

$logger->end($interval);
return $pages;