<?php
return [
    'commands' => [
        'migrate' => \Plumbus\Core\Command\Migrate::class,
        'parse' => \Books\Command\Parse::class,
        'search' => \Books\Command\Search::class,
        'reparse' => \Books\Command\UpdateBooks::class,
    ],
];
